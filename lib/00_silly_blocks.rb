def reverser(&prc)
  string = prc.call
  reversed = []

  string.split.each do |word|
    reversed << word.reverse
  end
  reversed.join(' ')
end

def adder(add = 1)
  add + yield
end

def repeater(num_times = 1, &prc)
  num_times.times do
    prc.call
  end
end

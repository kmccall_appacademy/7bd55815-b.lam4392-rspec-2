def measure(num_times = 1)
  run_times = []

  num_times.times do
    start_time = Time.now
    yield
    run_times << Time.now - start_time
  end

  run_times.reduce(:+) / run_times.length
end
